import { storiesOf } from "@storybook/vue";

import HeaderBlock from "src/components/organisms/HeaderBlock/HeaderBlock.vue";

storiesOf("molecules/HeaderBlock", module).add("default", () => ({
  components: { HeaderBlock },
  template: "<header-block></header-block>"
}));
