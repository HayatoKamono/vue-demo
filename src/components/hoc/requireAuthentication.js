const requireAuthentication = wrappedComponent => {
  return {
    render(createElement) {
      return createElement(wrappedComponent, {}, "hello world");
    }
  };
};

export default requireAuthentication;
