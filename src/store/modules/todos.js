const state = {
  todos: []
};

const getters = {
  completeTodos: state => {
    return state.todos;
  },
  incompleteTodos: state => {
    return state.todos;
  }
};

const mutations = {
  addTodo: (state, payload) => {
    state.todos = state.todos;
  },
  removeTodo: (state, payload = 1) => {
    state.todos = state.todos;
  }
};

const actions = {
  addTodo: ({ commit }, payload) => {
    commit("addTodo");
  },
  removeTodo: ({ commit }, payload) => {
    commit("removeTodo");
  }
};

export default {
  state,
  getters,
  mutations,
  actions
};
