import { storiesOf } from "@storybook/vue";

import BasicInput from "@/components/atoms/BasicInput/BasicInput.vue";

storiesOf("atoms/BasicInput", module).add("default", () => ({
  components: { BasicInput },
  template: "<basic-input></basic-input>"
}));
