/*
function mapDispatchToProps(dispatch) {
    return({
        sendTheAlert: () => {dispatch(ALERT_ACTION)}
    })
}

function mapStateToProps(state} {
    return({fancyInfo: "Fancy this:" + state.currentFunnyString})
}
*/

const connectWithStore = (
  mapStateToProps,
  mapDispatchToProps
) => wrappedComponent => {
  return {
    render(createElement) {
      return createElement(wrappedComponent, {}, "hello world");
    }
  };
};

export default connectWithStore;
