import { storiesOf } from "@storybook/vue";

import BasicButton from "@/components/atoms/BasicButton/BasicButton.vue";

storiesOf("atoms/BasicButton", module).add("default", () => ({
  components: { BasicButton },
  template: "<basic-button label='click'></basic-button>"
}));
