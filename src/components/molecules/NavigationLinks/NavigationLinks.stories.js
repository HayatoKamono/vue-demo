import { storiesOf } from "@storybook/vue";

import BasicNavigation from "src/components/molecules/NavigationLinks/NavigationLinks.vue";

storiesOf("atoms/NavigationLinks", module).add("default", () => ({
  components: { BasicNavigation },
  template: "<basic-navigation></basic-navigation>"
}));
