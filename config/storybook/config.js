/* eslint-disable import/no-extraneous-dependencies */
import { configure } from "@storybook/vue";

const req1 = require.context("../../src/components", true, /.stories.js$/);
const req2 = require.context("../../src/pages", true, /.stories.js$/);

function loadStories() {
  req1.keys().forEach(filename => req1(filename));
  req2.keys().forEach(filename => req2(filename));
}

configure(loadStories, module);
