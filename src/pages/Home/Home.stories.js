import { storiesOf } from "@storybook/vue";

import Home from "@/pages/Home/Home.vue";

storiesOf("pages/Home", module).add("default", () => ({
  components: { Home },
  template: "<home></home>"
}));
