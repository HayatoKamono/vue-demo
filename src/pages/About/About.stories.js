import { storiesOf } from "@storybook/vue";

import About from "@/pages/About/About.vue";

storiesOf("pages/About", module).add("default", () => ({
  components: { About },
  template: "<about></about>"
}));
